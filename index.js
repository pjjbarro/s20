//Mini-Activity

let assets = `[
	{
		id: "item-1",
		name: "Aircon",
		description: "Split type",
		stock: 4,
		isAvailable: true,
		dateAdded: "June 17, 2019"
	},
	{
		id: "item-2",
		name: "Washing Machine",
		description: "With dryer",
		stock: 3,
		isAvailable: true,
		dateAdded: "August 14, 2020"
	},


]`

//JSON - Javascript Object Notation

	//JSON is a string but formatted as JS objects
	//JSON is popularly used to pass data from one application to another
	//JSON is not only in JS but also in other programming langauges to pass data.
	//This is why it is specified as Javascript Object Notation
	//There are JSON that are saved in a file with extension called .json
	//There is a way to turn JSON as JS Objects and there is way to turn JS Objects to JSON



	//JS Objects are not the same as JSON
		//JSON is a string
		//JS object is a object
		//JSON keys are surrounded by double quotes

		/*
		Syntax for JSON
			{
				"key1": "valueA",
				"key2": "valueB"
			}

		*/

	//Converting JS Objects into JSON
		//This will allow us to convert/turn JS Objects into JSON
		//This is how we are going to manipulate our JSON
		//By first turning our JS objects into JSON, we can also turn JSON back into JS Objects.
		//This is commonly used when trying to pass data from one application to another via the use of HTTP requests. HTTP requests are request for resource between a server and a client (browser.)

		//JSON format is also used in databases.

		let batches = [{batchName: 'Batch 123'},{batchName: 'Batch 125'}];
		console.log(JSON.stringify(batches));

		let JSONarray = JSON.stringify(members);
		console.log(JSONarray);


		/*//Can you still use JS array methods on JSON arrays?
		//No. JSON is a string type format.
		JSONarray.push({
			username: "Yoh Asakura",
			password: "shamanKingButNotReally"
		})

		console.log(JSONarray);
*/
		let JSONparsed = JSON.parse(JSONarray);
		console.log(JSONparsed);
		JSONparsed.push({
			username: "Yoh Asakura",
			password: "xlawsaresecretlyWeak"
		})
		console.log(JSONparsed);

		JSONarray = JSON.stringify(JSONparsed);
		console.log(JSONarray);