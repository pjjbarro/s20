//We can aslo have an array of objects. We can group together objects in an array. That array can also use array methods for the objects. However, being objects are more complex than strings or numbers, there is a difference when handling them.

let users = [

	{
		name: "Mike Shell",
		username: "mikeBlueShell",
		email: "mikeShell01@gmail.com",
		password: "iammikey1999",
		isActive: true,
		dateJoined: "August 8, 2011"
	},
	{
		name: "Jake Janella",
		username: "jjnella99",
		email: "jakejanella_99@gmail.com",
		password: "jakiejake12",
		isActive: true,
		dateJoined: "January 14, 2015"
	}
];

//Much like a regular array, we can actually access this array of objects the same way.

console.log(users[0]);

//How can we access the properties of our objects in an array?

//Show the second items email property:
console.log(users[1].email);

//Can we update the properties of objects in an array?

//Mini-activity

users[0].email = "mikeKingOfShell@gmail.com";
users[1].email = "janellajakeArchitect@gmail.com";

console.log(users);

//Can we also use array methods for array of objects?

//Add another user into the array:

users.push({

	name: "James Jameson",
	username: "iHateSpidey",
	email: "jamesJjameson@gmail.com",
	password: "spideyisamenac64",
	isActive: true,
	dateJoined: "February 14, 2000"
});

class User{
	constructor(name,username,email,password){
		this.name = name;
		this.users = username;
		this.email = email;
		this.password = password;
		this.isActive = true;
		this.dateJoined = "September 28, 2021";
	}
}

let newUser1 = new User("Kate Middletown","notTheDuchess","seriouslyNotDuchess@gmail.com","notRoyaltyAtAll");

console.log(newUser1);

users.push(newUser1);

console.log(users);

//find()

function login(username,password){

	//check if the username does exist or any user uses that username
	//check if the password given matches the password of our user in the array.
	let userFound = users.find((user)=>{

		//user is a parameter which recieves each items in the users array and the users array is an array of objects. Therefore, we can expect that the user parameter will contain an object.

		return user.username === username && user.password === password
	})

	console.log(userFound);

	//Mini-Activity
	if (userFound){

		alert("Thank you for logging in.")
	} else {
		alert("Login Failed. Invalid Credentials.")
	}

}

login ("notTheDuchess","notRoyaltyAtAll");


let courses = [];

//Activity

class Course {
	constructor(id,name,description,price,isActive){
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.isActive = true;
	}	

}

const postCourse = (id,name,description,price) => {
	courses.push({

		id: id,
		name: name,
		description: description,
		price: price,
		isActive: true
	})
	alert ('You have created ${name}. The price is ${price}.')
};

const getSingleCourse = (id) => {
	let foundCourse = courses.find((course)=>id === course.id);

	return (foundCourse);
}

const deleteCourse = () => courses.pop();

